<?php

function Check_Number($num)
{
    if ($num > 1) {
        throw new Exception("The Number is Greater than 1");
    }
}

try {
    Check_Number(.4);
    echo "Opss! This number is smaller than 1";
} catch (Exception$e) {
    echo $e->getMessage();
}
?>